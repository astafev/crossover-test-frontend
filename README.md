Compiling
===============

`mvn install` will compile project and create `frontend-0.0.1-SNAPSHOT-jar-with-dependencies.jar` in target directory.


Running
===============

You can either run it with maven or launch executable jar.

1. To run with maven type `mvn exec:java`
2. To run jar. You need launch executable.
For example run this command from command line:
`java -jar frontend-0.0.1-SNAPSHOT-jar-with-dependencies.jar`