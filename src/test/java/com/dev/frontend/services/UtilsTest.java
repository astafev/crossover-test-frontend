package com.dev.frontend.services;

import com.dev.frontend.domain.Product;
import com.dev.frontend.domain.ServiceResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

public class UtilsTest {

    @Test
    public void assertToStringFormat() {
        assertEquals("", Utils.toString(null));
        assertEquals("123", Utils.toString(new Integer("123")));
        assertEquals("123", Utils.toString(new BigDecimal("123")));
        BigDecimal decimal = new BigDecimal(34.5);
        assertEquals(decimal.toString(), Utils.toString(decimal));
    }

    @Test
    public void testReadListServiceResponseWithProductList() throws IOException {
        String productList = "{\"error\":null,\"success\":true,\"result\":[{\"code\":\"code\",\"description\":\"descr\",\"price\":123.12,\"quantity\":12345}]}";

        ServiceResponse<List<Product>> result = Utils.readListServiceResponse(Product.class, new StringReader(productList));
        assertTrue(result.success);
        assertTrue(result.error == null);
        assertTrue(result.result.size() == 1);
        Product product = result.result.get(0);
        assertEquals(product.getCode(), "code");
        assertEquals(product.getDescription(), "descr");
        assertEquals(product.getPrice(), new BigDecimal("123.12"));
        assertEquals(product.getQuantity(), Integer.valueOf(12345));
    }

    @Test
    public void testReadSingleObjectServiceResponseWithProduct() throws IOException {
        String productList = "{\"error\":null,\"success\":true,\"result\":{\"code\":\"code\",\"description\":\"descr\",\"price\":123.12,\"quantity\":12345}}";

        ServiceResponse<Product> result = Utils.readSingleObjectServiceResponse(Product.class, new StringReader(productList));
        assertTrue(result.success);
        assertTrue(result.error == null);
        Product product = result.result;
        assertEquals(product.getCode(), "code");
        assertEquals(product.getDescription(), "descr");
        assertEquals(product.getPrice(), new BigDecimal("123.12"));
        assertEquals(product.getQuantity(), Integer.valueOf(12345));
    }
    @Test
    public void testWriteAndRead() throws IOException {
        Product p = new Product();
        p.setCode("code");
        p.setDescription("descr");
        p.setPrice(new BigDecimal("123.12"));
        p.setQuantity(12345);
        ObjectMapper objectMapper = new ObjectMapper();
        ServiceResponse<Product> writeValue = new ServiceResponse<>();
        writeValue.success = true;
        writeValue.result = p;
        String serialized = objectMapper.writeValueAsString(writeValue);

        ServiceResponse<Product> result = Utils.readSingleObjectServiceResponse(Product.class, new StringReader(serialized));
        assertTrue(result.success);
        assertTrue(result.error == null);
        Product product = result.result;
        assertEquals(product.getCode(), "code");
        assertEquals(product.getDescription(), "descr");
        assertEquals(product.getPrice(), new BigDecimal("123.12"));
        assertEquals(product.getQuantity(), Integer.valueOf(12345));
    }
}
