package com.dev.frontend.panels.edit;

import javax.swing.*;

import com.dev.frontend.domain.ServiceResponse;
import com.dev.frontend.panels.BusinessPresenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class EditContentPanel extends JPanel implements
		BusinessPresenter {
	private static final long serialVersionUID = 4495835331993612718L;
	public static Logger log = LoggerFactory.getLogger(EditContentPanel.class);

	public abstract int getObjectType();

	public abstract String getCurrentCode();
	
	/**
	 * can be used if every field on frame is of type JTextField and if its name
	 * like txt&lt;fieldNamInObjectClassCapitalized>
	 * */
	/*public T guiToObject() {
		Field[] fields = this.getClass().getFields();
		try {
			T t = objectClass.newInstance();
			for (Field field : fields) {
				if (field.getType() == JTextField.class) {
					String classFieldName = Character.toLowerCase(field
							.getName().charAt(3))
							+ field.getName().substring(4);
					Field classField = objectClass.getField(classFieldName);
					classField.set(t, ((JTextField) field.get(this)).getText());
				}
			}

		return t;
		} catch (ReflectiveOperationException e) {
			throw new RuntimeException("wrong usage", e);
		}

	}*/

	public boolean bindToGUI(Object o) {
		if (o instanceof ServiceResponse) {
			ServiceResponse response = (ServiceResponse) o;
			if (!response.success) {
				JOptionPane.showMessageDialog(null, "Error text:\n" + response.error, "Encountered error from service", JOptionPane.ERROR_MESSAGE);
				return false;
			} else return true;
		}
		if (o != null)
			log.warn("unknown object binding to GUI: " + String.valueOf(o));
		return false;
	}
}
