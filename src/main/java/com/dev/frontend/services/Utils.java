package com.dev.frontend.services;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;

import com.dev.frontend.domain.ServiceResponse;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.swing.*;

public class Utils {

    public static Double parseDouble(String value) {
        if (value == null || value.isEmpty())
            return 0D;
        try {
            return Double.parseDouble(value);
        } catch (Exception e) {
            return 0D;
        }
    }

    /**
     * search for property in system properties then in config.properties
     *
     * @return property value or <code>null</code>
     */
    public static String getProperty(String key) {

        String result = System.getProperty(key);
        if (result == null) {
            Properties properties = new Properties();
            InputStream resourceAsStream = Utils.class.getClassLoader().getResourceAsStream("config.properties");
            if (resourceAsStream != null) {
                try {
                    properties.load(resourceAsStream);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                result = properties.getProperty(key);
            }
        }
        return result;
    }


    private static ObjectMapper mapper = new ObjectMapper();

    public static ServiceResponse post(URL url, Object o) throws IOException {
        byte[] content = mapper.writeValueAsString(o).getBytes(Charset.forName("UTF-8"));
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "*/*");
        connection.setRequestProperty("Content-Length", String.valueOf(content.length));
        OutputStream os = connection.getOutputStream();
        os.write(content);
        os.flush();
        os.close();

        BufferedReader reader =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
        ServiceResponse response = mapper.readValue(reader, ServiceResponse.class);
        return response;
    }

    public static <T> ServiceResponse<List<T>> list(URL url, Class<T> expectedType) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "*/*");
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
        ServiceResponse<List<T>> response = readListServiceResponse(expectedType, reader);
        return response;
    }

    static <T> ServiceResponse<List<T>> readListServiceResponse(Class<T> expectedType, Reader reader) throws IOException {
        JavaType inner = mapper.getTypeFactory().constructCollectionType(List.class, expectedType);
        JavaType needed = mapper.getTypeFactory().constructParametrizedType(ServiceResponse.class, ServiceResponse.class, inner);
        return mapper.readValue(reader, needed);
    }

    /**
     * @param expectedType type of result property in ServiceResponse
     */
    public static <T> ServiceResponse<T> get(URL url, Class<T> expectedType) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "*/*");
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
        return readSingleObjectServiceResponse(expectedType, reader);
    }

    static <T> ServiceResponse<T> readSingleObjectServiceResponse(Class<T> expectedType, Reader reader) throws IOException {
        JavaType responseType = mapper.getTypeFactory().constructParametrizedType(ServiceResponse.class, ServiceResponse.class, expectedType);
        return mapper.readValue(reader, responseType);
    }

    public static ServiceResponse delete(URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("DELETE");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "*/*");
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(connection.getInputStream()));
        ServiceResponse response = mapper.readValue(reader, ServiceResponse.class);
        return response;
    }

    public static void showError(Object message, String title) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }

    public static String toString(Number b) {
        if (b == null)
            return "";
        return String.valueOf(b);
    }

    public static String urlEncode(String urlPart) throws UnsupportedEncodingException {
        return URLEncoder.encode(urlPart, "UTF-8").replaceAll("\\+", "%20");
    }
}
