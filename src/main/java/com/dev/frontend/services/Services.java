package com.dev.frontend.services;

import java.io.*;
import java.net.ConnectException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import com.dev.frontend.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dev.frontend.panels.ComboBoxItem;

public class Services {
    public static Logger log = LoggerFactory.getLogger(Services.class);

    public static final int TYPE_PRODUCT = 1;
    public static final int TYPE_CUSTOMER = 2;
    public static final int TYPE_SALESORDER = 3;

    public static String getObjectUrl(int objectType) {
        switch (objectType) {
            case TYPE_PRODUCT:
                return "/products";
            case TYPE_CUSTOMER:
                return "/customers";
            case TYPE_SALESORDER:
                return "/orders";
            default:
                return null;
        }
    }

    public static Class<? extends AbstractDomainObject> getObjectClass(int objectType) {
        switch (objectType) {
            case TYPE_PRODUCT:
                return Product.class;
            case TYPE_CUSTOMER:
                return Customer.class;
            case TYPE_SALESORDER:
                return OrderSales.class;
            default:
                return null;
        }
    }

    /**
     * This method is called eventually after you click save on any edit screen
     * object parameter is the return object from calling method guiToObject on edit screen
     * and the type is identifier of the object type and may be TYPE_PRODUCT ,
     * TYPE_CUSTOMER or TYPE_SALESORDER
     */
    public static Object save(Object object, int objectType) {
        try {
            URL url = new URL(getServiceUrl() + getObjectUrl(objectType));

            ServiceResponse response = Utils.post(url, object);
            log.debug("got response: {}", response);

            return response;

        } catch (IOException | IllegalArgumentException e) {
            throw new RuntimeException("unexpected error", e);
        }
    }

    /**
     * This method is called when you select record in list view of any entity
     * and also called after you save a record to re-bind the record again
     * the code parameter is the first column of the row you have selected
     * and the type is identifier of the object type and may be TYPE_PRODUCT ,
     * TYPE_CUSTOMER or TYPE_SALESORDER
     */
    public static Object readRecordByCode(String code, int objectType) {
        try {
            URL url = new URL(getServiceUrl() + getObjectUrl(objectType) + "/" + Utils.urlEncode(code));

            ServiceResponse response = Utils.get(url, getObjectClass(objectType));
            log.debug("got response: {}", response);

            return response;

        } catch (IOException | IllegalArgumentException e) {
            throw new RuntimeException("unexpected error", e);
        }
    }

    /**
     * This method is called when you select record in list view of any entity
     * and also called after you save a record to re-bind the record again
     * the code parameter is the first column of the row you have selected
     * and the type is identifier of the object type and may be TYPE_PRODUCT ,
     * TYPE_CUSTOMER or TYPE_SALESORDER
     */
    public static boolean deleteRecordByCode(String code, int objectType) {
        try {
            URL url = new URL(getServiceUrl() + getObjectUrl(objectType) + "/" + Utils.urlEncode(code));

            ServiceResponse response = Utils.delete(url);

            log.debug("got response: {}", response);

            return response.success;

        } catch (IOException | IllegalArgumentException e) {
            throw new RuntimeException("unexpected error", e);
        }
    }

    /**
     * This method is called when you open any list screen and should return all records of the specified type
     */
    public static List<Object> listCurrentRecords(int objectType) {
        try {
            URL url = new URL(getServiceUrl() + getObjectUrl(objectType));

            ServiceResponse response = Utils.list(url, getObjectClass(objectType));
            log.debug("got response: {}", response);

            @SuppressWarnings("unchecked")
            List<Object> result = (List<Object>) response.result;
            return result;

        } catch (ConnectException e) {
            Utils.showError("Can't connect to service\n" + e.getMessage(), "ConnectExcpetion");
            throw new RuntimeException("unexpected error", e);
        } catch (IOException | IllegalArgumentException e) {

            throw new RuntimeException("unexpected error", e);
        }
    }

    /**
     * This method is called when a Combo Box need to be initialized and should
     * return list of ComboBoxItem which contains code and description/name for all records of specified type
     */
    public static List<ComboBoxItem> listCurrentRecordRefernces(int objectType) {
        List<Object> objList = listCurrentRecords(objectType);
        List<ComboBoxItem> result = new ArrayList<>(objList.size());
        exit:
        for (Object o : objList) {
            ComboBoxItem item;
            switch (objectType) {
                case TYPE_PRODUCT:
                    Product p = (Product) o;
                    item = new ComboBoxItem(p.getCode(), p.getDescription());
                    break;
                case TYPE_CUSTOMER:
                    Customer c = (Customer) o;
                    item = new ComboBoxItem(c.getCode(), c.getName());
                    break;
                default:
                    log.warn("unknown object type: " + objectType);
                    break exit;
            }
            result.add(item);
        }
        return result;
    }

    /**
     * This method is used to get unit price of product with the code passed as a parameter
     */
    public static double getProductPrice(String productCode) {
        @SuppressWarnings("unchecked")
        ServiceResponse<Product> o = (ServiceResponse<Product>) readRecordByCode(productCode, TYPE_PRODUCT);
        if (o.result != null) {
            return o.result.getPrice() == null ? -1.0 : o.result.getPrice().doubleValue();
        } else {
            return -1.0;
        }
    }

    public static String getServiceUrl() {
        return Utils.getProperty("service.address");
    }
}
