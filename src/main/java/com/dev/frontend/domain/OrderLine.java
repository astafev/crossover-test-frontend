package com.dev.frontend.domain;

import com.dev.frontend.services.Utils;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderLine implements AbstractDomainObject {

    private Integer id;

    private Product product;

    private Integer quantity;

    private BigDecimal unitPrice;

    private BigDecimal totalPrice;

    public OrderLine(){}
    public OrderLine(String productCode, Integer quantity, BigDecimal unitPrice, BigDecimal totalPrice) {
        this.setProduct(new Product());
        this.getProduct().setCode(productCode);
        this.setQuantity(quantity);
        this.setUnitPrice(unitPrice);
        this.setTotalPrice(totalPrice);
    }

    @Override
    public String[] toStringArray() {
        return new String[]{product.getCode(), Utils.toString(quantity),
                Utils.toString(unitPrice),
                Utils.toString(totalPrice) };
    }
}
