package com.dev.frontend.domain;

import com.dev.frontend.services.Utils;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(exclude = "orderLines")
public class OrderSales implements AbstractDomainObject {

    private String orderNumber;

    private BigDecimal totalPrice;

    private List<OrderLine> orderLines;

    private Customer customer;

    public OrderSales() {
        customer = new Customer();
        orderLines = new ArrayList<>();
    }
    @Override
    public String[] toStringArray() {
        return new String[]{
                orderNumber, customer.getName(), Utils.toString(totalPrice)
        };
    }
}
