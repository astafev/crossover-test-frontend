package com.dev.frontend.domain;

import com.dev.frontend.services.Utils;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Product implements AbstractDomainObject {

    private String code;

    private String description;

    private BigDecimal price;

    private Integer quantity;

    @Override
    public String[] toStringArray() {
        return new String[]{
                code, description, Utils.toString(price), Utils.toString(quantity)
        };
    }
}
