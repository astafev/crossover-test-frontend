package com.dev.frontend.domain;


public class ServiceResponse<T> {
	public boolean success;
	public String error;
	public T result;

	@Override
	public String toString() {
		return "ServiceResponse{" +
				"success=" + success +
				", error='" + error + '\'' +
				", result=" + result +
				'}';
	}
}
