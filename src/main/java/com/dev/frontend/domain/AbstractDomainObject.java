package com.dev.frontend.domain;

public interface AbstractDomainObject {
    public abstract String[] toStringArray();
}
