package com.dev.frontend.domain;

import com.dev.frontend.services.Utils;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Customer implements AbstractDomainObject {
    private String code;
    private String name;
    private String address;
    private String phone1;
    private String phone2;
    private BigDecimal creditLimit;
    private BigDecimal currentCredit;

    @Override
    public String[] toStringArray() {
        return new String[]{
                code, name, address, phone1 == null ? phone2 : phone1, Utils.toString(currentCredit)
        };
    }
}
